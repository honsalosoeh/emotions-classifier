from models.model import Model
import Config
from metrics import Metrics
from skimage import feature
from sklearn.model_selection import cross_validate
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
import tensorflow as tf
from keras.models import Sequential
from keras import layers
import numpy as np

class Classifier(Model):

    def __init__(self, classifier=None, rna="Simple"):
        self.rna = ( classifier == None )
        self.type = rna
        if self.rna:
            if self.type == "Simple":
                self.classifier = self.cnn()
            elif self.type == "VGG":
                pass
            elif self.type == "ResNet50":
                pass
            else:
                print("error")
                exit()
            self.callbacks = []
            self.callbacks.append(tf.keras.callbacks.EarlyStopping(monitor='val_loss', 
                min_delta=Config.MIN_DELTA, patience=Config.PATIENCE, 
                verbose=1, mode='min'))
            
            self.callbacks.append(tf.keras.callbacks.ModelCheckpoint(filepath='./pesos.h5', 
                monitor='val_loss',
                verbose=1, 
                save_best_only=True,
                mode='min'))
        else:
            self.classifier = classifier

    def cnn(self):
        model = Sequential()
        model.add(layers.Conv2D(32, (3, 3), activation='relu', \
            input_shape=(Config.SIZE[0], Config.SIZE[1], 3)))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.Conv2D(64, (3, 3), activation='relu'))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.Conv2D(64, (3, 3), activation='relu'))
        model.add(layers.Flatten())
        model.add(layers.Dense(64, activation='relu'))
        model.add(layers.Dense(Config.NC, activation='softmax'))
        model.compile(optimizer="adam", 
            loss=tf.keras.losses.CategoricalCrossentropy(),
            metrics=['accuracy'])

        #model.compile(optimizer='adam', 
        #        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        #        metrics=['accuracy'])
        return model

    def train(self, x, y):
        m = Metrics()
        m.initSeed()
        metrics = None
        if self.rna:
            for i in range(Config.ITERATIONS):
                X_train, X_test, Y_train, Y_test = \
                    train_test_split(x, y, test_size=Config.TEST_SPLIT, \
                    random_state=Config.SEED)
                
                Y_train = tf.keras.utils.to_categorical(Y_train, num_classes=Config.NC)
                Y_test = tf.keras.utils.to_categorical(Y_test, num_classes=Config.NC)

                print("X_train: " + str(X_train.shape))
                print("Y_train: " + str(Y_train.shape))

                progress = self.classifier.fit(X_train, Y_train, epochs=Config.EPOCHS, \
                    batch_size=Config.BATCH_TAM, validation_split=Config.VALIDATION_SPLIT, \
                    callbacks=self.callbacks)
                #h = self.cnn.fit(train_images, train_labels_, epochs=Config.epochs,
                    #batch_size=Config.batch_size, validation_data=(test_images, test_labels_),
                    #callbacks = [earlystop_callback])
                predict = self.classifier.predict(X_test, batch_size=32)
                m.log(np.argmax(Y_test, axis=1), np.argmax(predict,axis=1))
            
            m.calc_mean_dt() # Compute mean and typical deviation
            m.training_graph(progress)

        else:
            #metrics = cross_validate(self.classifier, x, y, cv=Config.cv, scoring=Config.metrics)
            metrics = cross_validate(self.classifier, x, y, cv=Config.CV, scoring=Config.metrics)
            metrics['nome'] = str(self.classifier)
            metrics['tiempo (s)'] = np.mean(metrics.pop('fit_time', None)); metrics.pop('score_time', None)
            metrics['accuracy'] = np.mean(metrics.pop('test_accuracy', None))
            metrics['precision_macro'] = np.mean(metrics.pop('test_precision_macro', None))
            metrics['recall_macro'] = np.mean(metrics.pop('test_recall_macro', None))
            metrics['precision_micro'] = np.mean(metrics.pop('test_precision_micro', None))
            metrics['recall_micro'] = np.mean(metrics.pop('test_recall_micro', None))
            metrics['f1_macro'] = np.mean(metrics.pop('test_f1_macro', None))
        return metrics

    def setClassifier(self, newClassifier):
        self.classifier = newClassifier


    def predict(self, img):
        print(obj)



