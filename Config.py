import os
SIZE = (112, 112)
#path = os.path.join(os.getcwd(), "dataset")
path = os.path.join(os.path.dirname(__file__), "dataset")
#path = os.path.join(os.getcwd(), "dataset")
NC = len(os.listdir(path)) # Numero de clases
TRAIN_ITEMS = 200 # Numero de imaxes que colle de cada clase para adestrar (Max 1200)

##############################
# Classic methods parameters #
##############################

#kNN
NEIGHBORS = 5
CV = 10
# SVM
KERNEL = "poly" # poly ou 
# LR
SOLVER = "lbfgs" # lbfgs ou
MAX_ITER = 10000
MULTI_CLASS = "multinomial"
#RF
N_ESTIMATORS = 10
# Falta AuC (area under curve)
metrics = ['accuracy', 'precision_macro', 'recall_macro', 'precision_micro', 'recall_micro', 'f1_macro']

##############################
# Neural Networks parameters #
##############################

# Training
SEED = None
ITERATIONS=3
NUM_DIM_ULTCAPA=1
BASE_ROUTE=""
LEARNING_RATE=0.002
EPOCHS=30
BATCH_TAM=16 # Potencia de dous!
TRAIN_SPLIT=0.8
VALIDATION_SPLIT=0.1
TEST_SPLIT=0.1
MIN_DELTA=0.002
PATIENCE=6
CLASES=["Estradas", "Edificios", "Libre"]
