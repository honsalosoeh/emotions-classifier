import os, cv2, Config
import numpy as np
import Config
import pandas as pd
from models.classifier import Classifier
from keras.utils import to_categorical
from skimage import feature
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC


def load_data(path):
    x = []; y = []
    for i,label in enumerate(os.listdir(path)[:100]):
        for file in os.listdir(os.path.join(path, label)):
            filepath = os.path.join(path,label,file)
            img = cv2.cvtColor(cv2.resize(cv2.imread(filepath), Config.SIZE), cv2.COLOR_BGR2GRAY)
            img = np.asarray(img).astype(np.float32) / 255.0
            x.append(img); y.append(i)

    y = np.argmax(to_categorical(y,num_classes=Config.NC), axis=1)
    return (np.array(x), np.array(y))


def write_file(metrics):
    df = pd.DataFrame()
    for metric in metrics:
        df = pd.concat([df, pd.DataFrame(metric, index = [metric['nome']])])
    df.to_excel('./Metricas.xlsx')


if __name__ == "__main__":
    x,y = load_data(Config.path)

    x = [ feature.hog(img, feature_vector=True) for img in x ]  

    model = Classifier(classifier=KNeighborsClassifier(Config.neighbors))
    r1 = model.train(x,y)
    #model.setClassifier(SVC(kernel=Config.kernel))
    #results2 = model.train(x,y)
    model.setClassifier(RandomForestClassifier(n_estimators=Config.n_estimators))
    r3 = model.train(x,y)
    model.setClassifier(LogisticRegression(penalty=None, solver=Config.solver, max_iter=Config.max_iter, multi_class=Config.multi_class))
    r4 = model.train(x,y)
    write_file([r1, r3, r4])



